function addTokens(input, tokens) {
    //Validare
    if (typeof input !== "string")
        throw new Error("Invalid input");
    //----------------------------------------------------------------------------

    //Validare length
    if (input.length < 6)
        throw new Error("Input should have at least 6 characters");
    //----------------------------------------------------------------------------

    //Validare tokens
    function validareTokens(tokens) {
        for (token of tokens) {
            let values = Object.values(token);
            if (values.length > 1 || typeof values[0] !== "string")//un token cu mai multe atribute:value
                return false;
        }
        return true;
    }
    if (!validareTokens(tokens))
        throw new Error("Invalid array format");
    //----------------------------------------------------------------------------

    // The actual app feature:
    if (input.includes('...') && input.match(/\.\.\./g).length === tokens.length)
        tokens.forEach(token => input = input.replace('...', "${"+Object.values(token)[0])+"}");
    return input;

    /* 
    Note: the following changes were made ONLY for the test to pass:
    -Line 4 : Error message should be "Input should be a string"
    -Line 27: I think I shouldn't use "${" and "}" there, 
            since I want to replace the dots with the value, not with ${value}
    */
}

const app = {
    addTokens: addTokens
}

module.exports = app;